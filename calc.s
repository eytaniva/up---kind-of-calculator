section .bss
    stack: resb 4*size                          ;debug with     p/x (char[20])stack
    node: resb 4
    copyNode: resb 4
    succActions: resb 4
    counter: resb 2
    isFirstFlag: resb 1
    node1: resb 4
    node2: resb 4
    sum: resb 1
    shiftY: resb 4
    byte1: resb 1
    byte2:  resb 1
    cFlag: resb 1
    remainder: resb 1
    quotient: resb 1

section .rodata
    size: equ 5
    stackOver: db "Error: Operand Stack Overflow",10,0
    Insufficient: db "Error: Insufficient Number of Arguments on Stack",10,0
    wrongValue: db "wrong Y value",10,0
    debugging:  db "DEBUG MESSAGE:",0
    charFormat: db "%c", 0
    hexFormat: db "%X", 0
    entry: db "calc: ",0


section .data
    stackp: dd 0                                ;points to top elem in stack
    junk: dd 0
    isNumFlag: db 1
    listStart: dd 0
    nextChar: db 0
    temp: dd 0
    debugFlag: db 0
    freeHolder: dd 0
    isEmptyStack: dd 1


section .text
  align 16
    global main
    extern printf
    extern malloc
    extern free
    extern getchar

%macro freeList 0                               ;free list pointed by ecx
    %%freeLoop:
        pop ecx                                 ;take arg passed to freeloop
        push ecx                                ;Push arg for free
        mov ecx,dword[ecx+1]                    ;take next addr to free
	mov dword[freeHolder],ecx
        call free
        pop dword [junk]                        ;clean esp
        push dword[freeHolder]                  ;pass next addr for next freeloop
        cmp dword[freeHolder],0                 ;check if next addr is nullptr              
        jne %%freeLoop
        pop dword[junk]
%endmacro

%macro printEntry 0
    push entry
    call printf 
    pop dword [junk]                            ;clean esp

%endmacro

%macro isBetween 3                              ;3 params. %1 is the number, %2 is low range and %3 is high range 
    mov ebx ,1
    mov ecx , 0
    cmp %1,%2
    cmovb ebx,ecx
    cmp %1,%3
    cmova ebx,ecx
%endmacro

%macro calcY 0                                          ;res in shiftY
    mov dword[shiftY],0
    mov dword[counter],0

    %%loopAndCount2:                           ;count the number of Push-es
        push ecx
        inc dword[counter]                  	;aka i++
        mov ecx, dword[ecx+1]               	;take next node
        cmp ecx, 0
        jne %%loopAndCount2

    %%ignoreZeros2:                            ;while we get '0' in the end we skip the print
        pop ecx
	dec dword[counter]              	; aka i--
        mov edx ,0
        mov dl, byte[ecx]                   	;take the char to print
        cmp dl,0                            	;if we found a number(not 0) we start printing
        jne %%stepin
       

        cmp dword[counter],0
        ja %%ignoreZeros2
	jmp %%finishCalcY
    %%step:					;calculate Y by shifting old value by 4 to the 							;left(x16) and adding new byte value
        pop ecx
	dec dword[counter]              	; aka i--
        %%stepin:
        mov eax,dword[shiftY]
        sal eax,4
        mov dword[shiftY],eax
        mov edx,0
        mov dl, byte[ecx]                   	;take the byte to add
        add dword[shiftY],edx
        cmp dword[shiftY],200                   ;check if Y is greater than 200
	ja %%finishPop
   
        cmp dword[counter],0
        jne %%step
	jmp %%finishCalcY
    %%finishPop:				;finish poping the member we pushed in loopAndCount2
	cmp dword[counter],0
	je %%finishCalcY
	pop ecx
        dec dword[counter]              	; aka i--
        jmp %%finishPop
    %%finishCalcY:
    cmp dword[shiftY],200                      ;check if Y is greater than 200
    ja printWrongValue
%endmacro

main:
    push ebp	
    
    mov ecx,dword[esp+8]			;take number of carg
    cmp ecx,1
    jbe skipDebug

    mov ecx,dword[esp+12]			;take the pointer ** argv
    mov ecx,dword[ecx+4]			;take pointer to second member in argv
    mov al,byte[ecx]				;take first byte in string
    cmp al,'-'
    jne skipDebug
    mov al,byte[ecx+1]				;take second byte in string
    cmp al,'d'
    jne skipDebug

    mov byte[debugFlag],1

    skipDebug:
                                                ;reset vars                                         
    mov dword[succActions],0
    mov word[counter],0
    mov word[isFirstFlag],1
    mov word[node1],0
    mov word[node2],0
    mov word[sum], 0
    mov byte[cFlag], 0
    call myCalc

    printResults:
    push eax                                    ;eax return value of number of operations
    push hexFormat
    call printf
    pop dword [junk]                            ;clean esp
    pop dword [junk]                            ;clean esp

    mov eax,10                                  ;print \n
    push eax                                    ;eax return value of number of operations
    push charFormat
    call printf
    pop dword [junk]                            ;clean esp
    pop dword [junk]                            ;clean esp

    pop ebp
    ret

myCalc:

    push ebp
   
    myCalcLoop:
    mov byte[byte1],0
    mov byte[byte2],0
    mov dword[node1],0
    mov dword[node2],0
    mov dword[node],0
    mov dword[copyNode],0
    mov word[counter],0
    mov word[isFirstFlag],1
    mov byte[cFlag], 0
    mov dword[shiftY], 0
    mov byte[sum], 0
    mov byte[remainder], 0
    mov byte[quotient], 0

    mov edx, dword [succActions]                ;counting actions
    inc edx
    cmp byte[isNumFlag],1                       ;if was not an operator
    cmove edx, dword [succActions]              ;reset to old value
    mov dword [succActions] ,edx
    
    printEntry

    call getInput                               ;returns pointer to linked list for the new input
    cmp byte[isNumFlag],1
    je isNumber

    cmp byte [eax],'+'-55
    je plusAction
    cmp byte [eax],'q'-55
    je quitGracefully
    cmp byte [eax],'p'-55
    je popAndPrint
    cmp byte [eax],'d'-55
    je duplicate
    cmp byte [eax],'^'-55
    je shift
    cmp byte [eax],'v'-55
    je negShift
    cmp byte [eax],'n'-55
    je nbits
    cmp byte [eax],'r'-55
    je squareRoot                               ;plus check for the letter 's'
                                                ;eax will point to the new result
    finishAction:

   


    isNumber:                                   ;try to insert new-number/operation-result to the stack
        mov ebx, size 
        dec ebx
        cmp dword [stackp],ebx              ;check if we have a full stack already
        ja printStackOverflow

        mov ebx, dword [stackp]
        mov dword [stack + ebx*4],eax
        inc dword [stackp]
	mov dword[isEmptyStack],0
        cmp byte[debugFlag],1
        jne myCalcLoop
        call printDebug
        
    jmp myCalcLoop 

    myCalcEnd:	
    pop ebp
    mov eax, dword[succActions]
    ret



printStackOverflow:
    push eax
    freeList

    push stackOver
    call printf 
    pop dword [junk]                    ;clean esp
    jmp myCalcLoop

printInsufficientNumOfArgs:
    push Insufficient
    call printf 
    pop dword [junk]                    ;clean esp
    jmp myCalcLoop

printWrongValue:
    push wrongValue
    call printf 
    pop dword [junk]                    ;clean esp
    jmp myCalcLoop

getInput:

    push ebp

    call getchar
    cmp eax,10                          ;check if \n
    je endOfInput

    isBetween eax,48,57                 ;result is in ebx
    cmp ebx,1
    je cont
    isBetween eax,65,70                 ;result is in ebx
    sub eax,7				;remove from byte the extra value between the A to 10
    cont:
        sub eax,48			;remove from byte the ascii value of '0'
        mov byte[isNumFlag] ,bl         ;take value we got from isBeetwen to check if number
        mov dword[temp],eax             ;res the eax value
        push dword 5
        call malloc
        pop dword[junk]
        mov dword[node],eax             ;eax point to new memory
        mov edx,dword[temp]             ; get the char back
        mov byte[eax],dl                ;store the new char in the first byte
        mov edx,0
        mov dword[eax+1],edx            ;reset the pointer to nullptr

    inputRest:
        call getchar
        cmp eax,10                      ;check if \n    
        je endOfInput

        isBetween eax,48,57             ;result is in ebx
        cmp ebx,1
        je cont2
        sub eax,7			;same as above

        cont2:
            sub eax,48			;same as above
            push eax
            push dword 5
            call malloc
            pop dword[junk]
            mov ecx,dword[node]         ;res the last node
            mov dword[node],eax         ;eax point to new memory
            pop edx
            mov byte[eax],dl            ;store the new char in the first byte
            mov dword[eax+1],ecx        ;reset the pointer to nullptr
            jmp inputRest

    endOfInput:	
    	pop ebp
        mov eax, dword [node]
        ret


printDebug:

    push ebp

    mov dword[counter],0
    push eax

    push debugging
    call printf
    pop dword [junk]                        ;clean

    pop eax

    loopAndCount3:                           ;count the number of Push-es
        push eax
        inc dword[counter]                  ;aka i++
        mov eax, dword[eax+1]               ;take next node
        cmp eax, 0
        jne loopAndCount3

    ignoreZeros3:                            ;while we get '0' in the end we skip the print
        pop eax
        mov dword[node],eax
        
        mov edx ,0
        mov dl, byte[eax]                   ;take the char to print
        cmp dl,0                            ;if we found a number(not 0) we start printing
        jne print3
    
        dec dword[counter]                  ;aka i--
        cmp dword[counter],0
        jne ignoreZeros3

        inc dword[counter]                  ;we insert 0 for print case: the list is 0-0-0-0-0...
        mov dl,0
        jmp print3


    loopPrint3:
        pop eax
        mov dword[node],eax
        mov edx ,0
        mov dl, byte[eax]                  ;take the char to print

        print3:
            push edx
            push hexFormat
            call printf
            pop dword [junk]                ;clean esp
            pop dword [junk]                ;clean esp
            
            dec dword[counter]              ; aka i--
            cmp dword[counter],0
            jne loopPrint3
        

    mov edx,10                              ;print \n
    push edx
    push charFormat
    call printf
    pop dword [junk]                        ;clean
    pop dword [junk]                        ;clean
	
    pop ebp
    ret

duplicate:
    push eax			 	    ;free the command
    freeList

    cmp dword [stackp],1
    jb printInsufficientNumOfArgs

    mov ebx, dword [stackp]
    dec ebx
    mov eax, dword [stack + ebx*4]




    mov dword[copyNode],eax

    push dword 5
    call malloc
    pop dword[junk]
    mov dword[node],eax                     ;eax point to new memory
    mov edx, dword[copyNode]                ; get the node back
    mov bl,byte[edx]
    mov byte[eax],bl                        ;store the new char in the first byte
    mov edx,0
    mov dword[eax+1],edx                    ;reset the pointer to nullptr
    mov dword[listStart],eax

    inputRest2:
        mov eax,dword[copyNode]             ;set eax to point to next node
        mov eax,dword[eax+1]                ;move to next node in the origin list
        mov dword[copyNode],eax

        cmp eax, 0
        je endOfInput2

        
        push dword 5
        call malloc
        pop dword[junk]

        mov ebx,dword[node]                 ;set next address fot old node in the new list
        mov dword[ebx+1],eax
        mov dword[node],eax

        mov ecx,dword[copyNode]
        mov dl, byte[ecx]
        mov byte[eax],dl
        mov edx,0
        mov dword[eax+1],edx                ;reset the pointer to nullptr
        jmp inputRest2

    endOfInput2:
        mov eax, dword [listStart]
        


    jmp finishAction

popAndPrint:
    push eax			 	    ;free the command
    freeList

    cmp dword [stackp],1
    jb printInsufficientNumOfArgs

    dec  dword [stackp]                     ;point to last mem in stack
    mov ebx, dword [stackp]
    mov eax, dword [stack + ebx*4]          ;eax hold the address for the next var to print
    mov dword[counter],0
    
    mov ecx,1
    mov edx,dword[isEmptyStack]
    cmp ebx,0
    cmove edx,ecx
    mov dword[isEmptyStack],edx

    loopAndCount:                           ;count the number of Push-es
        push eax
        inc dword[counter]                  ;aka i++
        mov eax, dword[eax+1]               ;take next node
        cmp eax, 0
        jne loopAndCount

    ignoreZeros:                            ;while we get '0' in the end we skip the print
        pop eax
        mov dword[node],eax
        
        mov edx ,0
        mov dl, byte[eax]                   ;take the char to print
        cmp dl,0                            ;if we found a number(not 0) we start printing
        jne print
       
        dec dword[counter]                  ;aka i--
        cmp dword[counter],0
        jne ignoreZeros

        inc dword[counter]                  ;we insert 0 for print case: the list is 0-0-0-0-0...
        mov dl,0
        jmp print


    loopPrint:
        pop eax
        mov dword[node],eax
        mov edx ,0
        mov dl, byte[eax]                  ;take the char to print

        print:
            push edx
            push hexFormat
            call printf
            pop dword [junk]                ;clean esp
            pop dword [junk]                ;clean esp
            
            dec dword[counter]              ; aka i--
            cmp dword[counter],0
            jne loopPrint


    skipLoopPrint:
    push dword[node]                        ;pass arg to freelist
    freeList                                ;free list is a macro: frees the list pointed by ecx

    mov edx,10                              ;print \n
    push edx
    push charFormat
    call printf
    pop dword [junk]                        ;clean
    pop dword [junk]                        ;clean
    ;add dword [succActions] ,1
    jmp myCalcLoop


nbits:
    push eax			 	    ;free the command
    freeList

    cmp dword [stackp],1
    jb printInsufficientNumOfArgs

    dec dword [stackp]                      ;point to pre last stack element
    mov ebx,dword[stackp]
    mov ecx, dword [stack + ebx*4]
    nbitsLoop:
        mov edx,0                           ;reset
        mov eax,0
        mov dl,byte [ecx]                   ;set edx to have ecx value(Popcnt doesnt work with 8bit)
        popcnt ax,dx                        ;set edx to num of 1 in current byte in node
        add word[counter],ax                ;add edx to counter
        mov ecx , dword [ecx + 1]
        cmp ecx,0
        jne nbitsLoop
    
    mov ebx,dword[stackp]                   ;free the list
    mov ecx, dword [stack + ebx*4]
    push ecx 
    freeList                           
    ;pop dword[junk]                         ;end free

    push dword 5                                  ; we need 2 byte -> 2 nodes for max number of 1 in 80 chars 4*80 = 320
    call malloc
    pop dword[junk]
    mov dword[temp],eax                             ;ebx point to new memory
	
    push dword 5
    call malloc
    pop dword[junk]

    mov ebx,dword[temp]

    mov dl,byte[counter+1]                ;take bigger byte in counter to ebx node
    mov byte[ebx],dl
    mov dword[eax+1],ebx                ;set eax node to point to ebx node
    mov dl,byte[counter]              ;take bigger byte in counter to eax node
    mov byte[eax],dl
    mov edx,0
    mov dword[ebx+1], edx                 ;set ebx node to point to nullptr
    mov word[counter],0                 ;reset counter
    jmp finishAction				;result at eax

quitGracefully:
    push eax			 	    ;free the command
    freeList

    cmp dword[isEmptyStack],1
    je myCalcEnd

    loopQuitFree:
	    dec dword [stackp]                  ;point to pre last stack element
	    mov ebx,dword[stackp]
	    cmp ebx,0
	    jl myCalcEnd

	    ;dec ebx
	    mov ecx, dword [stack + ebx*4]
	    push ecx
	    freeList

	    
	    jmp loopQuitFree


plusAction:
    push eax			 	    ;free the command
    freeList

    cmp dword [stackp],2
    jb printInsufficientNumOfArgs

    mov edx, dword [stackp]  

    dec edx                                     ;point to pre last stack element
    mov ebx,dword[stack + edx*4]                ;point ebx to first list
    dec edx
    mov ecx,dword[stack + edx*4]                ;point ecx to second list

    mov dword[node1],ebx
    mov dword[node2],ecx
    call addLists
                                                ;res in listStart


    mov byte[isFirstFlag], 1                    ;reset isFirstFlag

    mov edx, dword [stackp]  
    dec edx                                     ;point to last stack element
    mov ebx,dword[stack + edx*4]                ;point ebx to first list
    push ebx
    freeList

    mov edx, dword [stackp]  
    dec edx                                     ;point to pre last stack element
    dec edx
    mov ecx,dword[stack + edx*4]                ;point ecx to second list
    push ecx
    freeList            

    dec dword[stackp]
    dec dword[stackp]

    mov eax,dword[listStart]                    ;check

    jmp finishAction                            ;result from addLists is already in eax


addLists:

    push ebp

    addListsLoop:
    cmp byte[isFirstFlag], 1                    ;add list starting with ebx with lst starting with ecx
    je firstAdd                                 ;reset node


    mov ebx,dword[node1]                    ;take 1 arg passed to func
    mov ecx,dword[node2]                    ;take 2 arg passed to func

    
    cmp ebx,0
    je ebxNull
    cmp ecx,0
    je ecxNull
    jmp regular

    ebxNull:
        cmp ecx,0
        je checkCarry

        mov cl, byte[ecx]
        mov byte[byte1],0
        mov byte[byte2],cl
        
        call add2bytes
        
        mov eax,dword[node]
        mov edx,dword[copyNode]
        mov dword[edx+1],eax
        mov dword[copyNode],eax
        
        mov ecx,dword[node2]                    ;take 2 arg passed to func
        mov ecx , dword [ecx + 1]               ;move to next node in other list
        mov dword[node2],ecx
        jmp addListsLoop

    ecxNull:
        cmp ebx,0
        je checkCarry

        mov bl, byte[ebx]
        mov byte[byte1],0
        mov byte[byte2],bl
        
        call add2bytes
        
        mov eax,dword[node]
        mov edx,dword[copyNode]
        mov dword[edx+1],eax
        mov dword[copyNode],eax
        
        mov ebx,dword[node1]                    ;take 2 arg passed to func
        mov ebx , dword [ebx + 1]               ;move to next node in other list
        mov dword[node1],ebx
        jmp addListsLoop

    regular:
        mov ebx,dword[node1]                    ;take 1 arg passed to func
        mov ecx,dword[node2]                    ;take 2 arg passed to func
        mov bl, byte[ebx]
        mov cl, byte[ecx]
        mov byte[byte1],bl
        mov byte[byte2],cl
        
        call add2bytes
        mov eax,dword[node]
        mov edx,dword[copyNode]
        mov dword[edx+1],eax
        mov dword[copyNode],eax
        
        mov ebx,dword[node1]                    ;take 1 arg passed to func
        mov ecx,dword[node2]                    ;take 2 arg passed to func
        mov ebx , dword [ebx + 1]               ;move to next node in one list
        mov ecx , dword [ecx + 1]               ;move to next node in other list
        mov dword[node1],ebx
        mov dword[node2],ecx

        mov byte[isFirstFlag], 0                ;mark that we did the first add
        
        jmp addListsLoop

    firstAdd:                                   ;first addition is with add(not adc)
        
        mov ebx,dword[node1]                    ;take 1 arg passed to func
        mov ecx,dword[node2]                    ;take 2 arg passed to func

        mov bl, byte[ebx]



        mov cl, byte[ecx]
        mov byte[byte1],bl
        mov byte[byte2],cl
        
        call add2bytes

        mov eax,dword[node]
        mov edx,0
        mov dword[eax+1],edx
        mov dword[listStart],eax
        mov dword[copyNode],eax
        
        mov ebx,dword[node1]                    ;take 1 arg passed to func
        mov ecx,dword[node2]                    ;take 2 arg passed to func
        mov ebx , dword [ebx + 1]               ;move to next node in one list
        mov ecx , dword [ecx + 1]               ;move to next node in other list
        mov dword[node1],ebx
        mov dword[node2],ecx

        mov byte[isFirstFlag], 0                ;mark that we did the first add
        
        jmp addListsLoop
    
    

    checkCarry:
        cmp byte[cFlag],0                       ;both lists are finished but there is still a carry
        je return

        mov byte[byte1],0
        mov byte[byte2],0

        call add2bytes

        mov edx,dword[copyNode]
        mov dword[edx+1],eax



    return:	
    	pop ebp
        ret


add2bytes:                                          ;uses carryFlag + byte1 and byte2 and sets node to the result

    push ebp

    mov ecx, 0
    mov edx, 0
    mov cl, byte[byte1]
    mov bl, byte[byte2]
    add cl,bl
    add cl,byte[cFlag]                              ;add carry from prev add2bytes operation
    mov byte[sum],cl

    push dword 5                                          ;create new node for result
    call malloc
    pop dword[junk]
    mov dword[node],eax

    cmp byte[sum],15				;if byte is higher than 15 there is an overflow
    ja lightFlag

    mov byte[cFlag],0
    jmp finishAdd

    lightFlag:
        mov byte[cFlag],1			;light overflow flag
        sub byte[sum],16			;remove overflow part

    finishAdd:					;move the new value to the node we created
        mov bl,byte[sum]
        mov eax,dword[node]
        mov byte[eax],bl
	mov edx,0
        mov dword[eax+1],edx
	
    pop ebp
    ret

shift:  ;x*(2^y)
    push eax			 	    ;free the command
    freeList

    cmp dword [stackp],2
    jb printInsufficientNumOfArgs

    mov edx, dword [stackp]  

    dec edx                                     ;point to pre last stack element
    dec edx
    mov ecx,dword[stack + edx*4]                ;point ecx to second list
	;calculate shiftY
    calcY                                   


    mov edx, dword [stackp]  
    dec edx                                     ;point to pre last stack element
    mov ecx,dword[stack + edx*4]                ;point ecx to second list
    mov dword[listStart],ecx

    addYTimes:					;send X to addLists Y times, each time X is updated 							;to new value from prev addLists
        cmp dword[shiftY],0
        jbe endShift

        mov ecx,dword[listStart]
        mov dword[node1],ecx
        mov dword[node2],ecx
        mov dword[temp],ecx
        mov byte[isFirstFlag], 1                ;reset isFirstFlag
        call addLists

        mov byte[isFirstFlag], 1                ;reset isFirstFlag

        mov edx, dword [temp]  
        push edx
        freeList				;free prev X list

        dec dword[shiftY]
        jmp addYTimes
        
    

    endShift:					;free the original Y
        mov edx, dword [stackp]  
        dec edx                                 ;point to pre last stack element
        dec edx
        mov ecx,dword[stack + edx*4]            ;point ecx to second list
        push ecx
        freeList            

        dec dword[stackp]
        dec dword[stackp]

        mov eax,dword[listStart]                ;move result to eax
        jmp finishAction
    

negShift:
    push eax			 	    	;free the command
    freeList

    cmp dword [stackp],2
    jb printInsufficientNumOfArgs

    mov edx, dword [stackp]  

    dec edx                                     ;point to pre last stack element
    dec edx
    mov ecx,dword[stack + edx*4]                ;point ecx to second list
	;calculate shiftY
    calcY

    mov edx, dword [stackp]  
    dec edx                                     ;point to pre last stack element
    mov ecx,dword[stack + edx*4]                ;point ecx to second list
    mov dword[temp],ecx

    mov eax, 0
    mov ax,word[shiftY]

    mov bl,4
    div bl					;divide al by bl and store result in ax

    mov byte[remainder],ah			;remainder as in 14/4=2
    mov byte[quotient],al			;quoteint as in 14/4=3 ->3*4+2=14

    mov ecx,dword[temp]
    mov dword[listStart],ecx

    loopQue:					;while quo is above 1 we move forward(plan to ignore the node)
        cmp byte[quotient],0
        je setStart
        cmp byte[quotient],1
        je resetNext

        dec byte[quotient]
        mov ecx,dword[ecx+1]
	cmp ecx,0
	je resultZero
        jmp loopQue
		
        resultZero:				;need to remove more nodes than there are as the Y is 							;too high for the X and the result needs to be 0
            push dword 5                                          ;create new node for result
            call malloc
            pop dword[junk]
            mov byte[eax],0			;set new node to have val 0
            mov dword[eax+1],0			;set new node to point to nullptr
            mov dword[temp],eax

            push dword[listStart]		;free origin X
            freeList
	    dec dword[stackp]
	    dec dword[stackp]
	    mov edx, dword [stackp]  
	    push dword[stack + edx*4]           ;free origin Y
	    freeList
	    mov eax,dword[temp]			;set the new node as answer
	    jmp finishAction
        resetNext:				;reset so the next node in old list will be null
            mov edx,dword[ecx+1]
            push ecx                            ;backup ecx
            cmp edx,0
            jne reg

            reg:
                pop ecx                         ;restore ecx
                mov dword[listStart],edx 
                mov dword[ecx+1],0
                push dword[temp]		;free old list till the node ecx to be replaced by edx
                freeList

    setStart:
        mov ebx,dword[listStart]

    shiftByPairs:
        mov ecx,dword[ebx+1]
        cmp ecx,0
        jne takeByte
        mov dl,0
        jmp doShift
        takeByte:
            mov dl,byte[ecx]                    ;take byte from nextNode(more significant)

        sal dl,4				;make space for next byte(less significant)
        
        doShift:
            add dl,byte[ebx]			;add the less signficant byte
            mov cl,byte[remainder]
            shr dl, cl                          ;shift by remainder
            sal dl,4                            ;set 0 at upper 4 bits
            shr dl,4                            ;set 0 at upper 4 bits
            mov byte[ebx],dl			;set new value of the less significant by new val

        mov ebx,dword[ebx+1]                    ;move to the next node and do again
        cmp ebx,0
        jne shiftByPairs

    dec dword[stackp]
    dec dword[stackp]
    mov edx, dword [stackp]  
    push dword[stack + edx*4]                		;free Y
    freeList

    mov eax,dword[listStart]
    jmp finishAction

squareRoot:
    push eax			 	    ;free the command
    freeList

    jmp finishAction


